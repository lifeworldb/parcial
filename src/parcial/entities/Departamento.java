/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcial.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sebastian.valencia
 */
public class Departamento {
    
    private String Nombre;
    private String Mapa;
    private List<Municipio> Municipios;
    
    public Departamento(String nombre, String mapa) {
        this.Nombre = nombre;
        this.Mapa = mapa;
        this.Municipios = new ArrayList();
    }

    public String getNombre() {
        return Nombre;
    }

    public String getMapa() {
        return Mapa;
    }

    public List<Municipio> getMunicipios() {
        return Municipios;
    }

    public void setMunicipios(Municipio Municipio) {
        this.Municipios.add(Municipio);
    }
    
}
