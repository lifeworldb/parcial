/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcial.entities;

/**
 *
 * @author sebastian.valencia
 */
public class Municipio {
    
    private String Departamento;
    private String Nombre;
    private String Capital;
    
    public Municipio(String departamento, String nombre, String capital) {
        this.Departamento = departamento;
        this.Nombre = nombre;
        this.Capital = capital;
    }

    public String getDepartamento() {
        return Departamento;
    }

    public String getNombre() {
        return Nombre;
    }

    public String isCapital() {
        return Capital;
    }
    
    
    
}
