/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcial;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import parcial.entities.Departamento;
import parcial.entities.Municipio;
import utils.Image;

/**
 *
 * @author sebastian.valencia
 */
public class controlFrm {
    
    List<Departamento> dep;
    List<Municipio> mun;
    
    File fileDep = new File(getClass().getResource("/resources/txt/Departamentos.csv").getFile());
    File fileMun = new File(getClass().getResource("/resources/txt/Municipios.txt").getFile());
    
    
    public controlFrm() {
        dep = new ArrayList();
        mun = new ArrayList();
    }
    
    public void CargarDepartamentos(JComboBox cbm) {
        //cbm.removeAllItems();
        //cbm.addItem("Selecciona un departamento.");
        String content;
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileDep));
            BufferedReader bm = new BufferedReader(new FileReader(fileMun));
            
            while((content = br.readLine()) != null) {
                //System.out.printf("Departamento %s, Mapa: %s \n", content.split(",")[0], content.split(",")[1]);
                dep.add(new Departamento(content.split(",")[0], content.split(",")[1]));
            }
            
            while((content = bm.readLine()) != null) {
                //System.out.println(content.split(",").length);
                //String capital = (content.split(",").length > 2) ? content.split(",")[2]:"";
                mun.add(new Municipio(content.split(",")[0], content.split(",")[1], (content.split(",").length > 2) ? content.split(",")[2]:""));
            }
            
            dep.forEach(val -> {
                cbm.addItem(val.getNombre());
                mun.forEach(muni -> {
                    if (muni.getDepartamento().equals(val.getNombre()))
                        val.setMunicipios(muni);
                });
            });
            
            //System.out.println(dep);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(controlFrm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(controlFrm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //System.out.println(dep.get(0).getMunicipios().get(0).getNombre());
    }
    
    public void CargarMunicipios(JTable tbl, int departamento) {
        //tbl.setValueAt(dep.get(0).getMunicipios().get(0).getNombre(), 0, 0);            
        if (departamento > 0 && ((DefaultTableModel) tbl.getModel()).getRowCount() > 0) {
            tbl.setModel(new DefaultTableModel(new Object[]{"Municipio","Capital"}, 0));
        }
        
        if (departamento > 0) {
            for (int i = 0; i < dep.get(departamento - 1).getMunicipios().size(); i++) {
                ((DefaultTableModel) tbl.getModel()).addRow(new Object[]{dep.get(departamento - 1).getMunicipios().get(i).getNombre(),dep.get(departamento - 1).getMunicipios().get(i).isCapital()});
            }
        }
    }
    
    public void CambiarMapa(JPanel pnl, int depart) {
        if (depart > 0) {
            pnl.removeAll();
            Image im = new Image(pnl,dep.get(depart - 1).getMapa());
            pnl.add(im).repaint();
        }
    }
}
