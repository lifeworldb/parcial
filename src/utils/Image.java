/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.Graphics;
import javax.swing.*;

/**
 *
 * @author sebastian.valencia
 */
public class Image extends javax.swing.JPanel {
    private int x, y;
    private String map;
    
    public Image(JPanel jpl, String map){
        this.x = jpl.getWidth();
        this.y = jpl.getHeight();
        this.map = map;
        this.setSize(x, y);
    }
    
    @Override
    public void paint(Graphics g){
        ImageIcon img = new ImageIcon(getClass().getResource("/resources/maps/"+map));
        g.drawImage(img.getImage(), 0, 0, x, y, null);
    }
}
